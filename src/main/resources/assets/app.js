var style = {
    table: {
        border: "1px solid black",
        textAlign: "left"
    },
    slettelink: {
        cursor: "pointer",
        textDecoration: "underline"
    }
};

var Meetup = React.createClass({
    render: function () {
        var meetup = this.props.meetup;
        return (
            <tr>
                <td style={style.table}>{meetup.eventDate}</td>
                <td style={style.table}>{meetup.group}</td>
                <td style={style.table}><a href={meetup.eventUrl}>{meetup.eventName}</a></td>
                <td style={style.table}><span style={style.slettelink} onClick={function() {this.props.removeMeetup(meetup.id)}.bind(this)}>[x]</span></td>
            </tr>
        )
    }
});

var MeetupList = React.createClass({
    render: function () {
        var removeMeetup = this.props.removeMeetup;
        return (
            <table style={style.table}>
                <thead>
                <tr>
                    <th style={style.table}>Dato</th>
                    <th style={style.table}>Meetupgruppe</th>
                    <th style={style.table}>Tema</th>
                    <th style={style.table}></th>
                </tr>
                </thead>
                <tbody>
                {this.props.meetups.map(function (meetup) {
                    return <Meetup key={meetup.id} meetup={meetup} removeMeetup={removeMeetup}/>;
                })}
                </tbody>
            </table>
        )
    }
});

var MeetupBox = React.createClass({
    loadMeetupsFromServer: function () {
        $.ajax({
            url: '/api/meetups',
            dataType: 'json',
            cache: false,
            success: function (data) {
                this.setState({meetups: data});
            }.bind(this),
            error: function (xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    removeMeetup: function (id) {
        var meetups = this.state.meetups;
        var indexToRemove = -1;
        for (var i = 0; i < meetups.length; i++) {
            if (meetups[i].id === id) {
                indexToRemove = i;
                break;
            }
        }
        meetups.splice(indexToRemove, 1);
        this.setState({meetups: meetups});
    },
    getInitialState: function () {
        return {meetups: []};
    },
    componentDidMount: function () {
        this.loadMeetupsFromServer();
    },
    render: function () {
        return (
            <div className="commentBox">
                <MeetupList meetups={this.state.meetups} removeMeetup={this.removeMeetup}/>
            </div>
        );
    }
});

ReactDOM.render(
    <MeetupBox />,
    document.getElementById('content')
);
