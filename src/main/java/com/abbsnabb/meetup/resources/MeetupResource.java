package com.abbsnabb.meetup.resources;

import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.abbsnabb.meetup.api.Meetup;
import com.abbsnabb.meetup.client.HttpMeetupClient;
import com.abbsnabb.meetup.client.MeetupClient;

@Path("meetups")
public class MeetupResource {

    private final MeetupClient meetupClient;

    public MeetupResource(MeetupClient meetupClient) {
        this.meetupClient = meetupClient;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Meetup> meetups() {
        List<String> blacklist = Arrays.asList("Byggekveld", "Saturday is hackurday!");
        return meetupClient.hentMeetups().stream()
                .filter(meetup -> !blacklist.contains(meetup.eventName))
                .collect(Collectors.toList());
    }

    @GET
    @Path("asCSV")
    public Response asCSV() {
        final List<Meetup> meetups = meetupClient.hentMeetups();
        StringBuilder sb = new StringBuilder("Dato;Meetupgruppe;Tema\n");
        meetups.forEach(n -> {
            sb.append(n.eventDate + ";" + n.group + ";" + n.eventName + "\n");
        });

        WeekFields weekFields = WeekFields.of(Locale.forLanguageTag("nb"));
        int weekNumber = LocalDate.now().get(weekFields.weekOfWeekBasedYear());

        final String filename = String.format("meetups.uke%s.csv", weekNumber);
        return Response.ok(sb.toString(), "text/csv;charset=utf-8")
                .header("Content-Disposition", "attachment;filename=" + filename)
                .build();
    }
}
