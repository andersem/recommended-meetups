package com.abbsnabb.meetup.client;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import com.abbsnabb.meetup.api.Meetup;
import com.fasterxml.jackson.databind.JsonNode;

public class HttpMeetupClient implements MeetupClient{

    private final WebTarget target;

    @Override
    public List<Meetup> hentMeetups() {
        final Response response = target.request().get();

        final JsonNode jsonNodes = response.readEntity(JsonNode.class);
        final JsonNode results = jsonNodes.get("results");
        final List<Meetup> meetups = new ArrayList<>();

        results.forEach(n -> {
            final String id = n.get("id").toString().replaceAll("\"", "");
            final String groupName = n.get("group").get("name").toString().replaceAll("\"", "");
            final String eventName = n.get("name").toString().replaceAll("\"", "");
            final Instant time = Instant.ofEpochMilli(n.get("time").asLong());
            final LocalDateTime eventDate = LocalDateTime.ofInstant(time, ZoneOffset.ofHours(0));
            final String formattedDate = eventDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
            final String eventUrl = n.get("event_url").toString().replaceAll("\"", "");

            meetups.add(new Meetup(id, formattedDate, groupName, eventName, eventUrl));
        });
        return meetups;
    }

    public HttpMeetupClient(String meetupurl) {
        final Client client = ClientBuilder.newClient();
        target = client.target(meetupurl);
    }
}
