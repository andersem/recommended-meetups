package com.abbsnabb.meetup.client;

import com.abbsnabb.meetup.api.Meetup;

import java.util.List;

public interface MeetupClient {
    List<Meetup> hentMeetups();
}
