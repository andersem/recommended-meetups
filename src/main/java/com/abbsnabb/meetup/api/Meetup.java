package com.abbsnabb.meetup.api;

public class Meetup {
    public final String id;
    public final String eventDate;
    public final String group;
    public final String eventName;
    public final String eventUrl;

    public Meetup(String id, String eventDate, String group, String eventName, String eventUrl) {
        this.id = id;
        this.eventDate = eventDate;
        this.group = group;
        this.eventName = eventName;
        this.eventUrl = eventUrl;
    }
}
