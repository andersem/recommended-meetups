package com.abbsnabb.meetup;

import com.abbsnabb.meetup.client.HttpMeetupClient;
import com.abbsnabb.meetup.resources.MeetupResource;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class MeetupAppApplication extends Application<MeetupAppConfiguration> {

    public static void main(final String[] args) throws Exception {
        if (args.length == 0) {
            new MeetupAppApplication().run(new String[] { "server", "localconfig.yml" });
        } else {
            new MeetupAppApplication().run(args);
        }
    }

    @Override
    public String getName() {
        return "MeetupApp";
    }

    @Override
    public void initialize(final Bootstrap<MeetupAppConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets/", "/", "index.html"));
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor()
                )
        );
    }

    @Override
    public void run(final MeetupAppConfiguration config,
            final Environment environment) {
        environment.jersey().register(new MeetupResource(new HttpMeetupClient(config.getMeetupurl())));
    }

}
