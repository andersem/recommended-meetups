package com.abbsnabb.meetup;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.*;
import javax.validation.constraints.*;

public class MeetupAppConfiguration extends Configuration {

    private String meetupurl;

    @JsonProperty
    public String getMeetupurl() {
        return meetupurl;
    }

    @JsonProperty
    public void setMeetupurl(String meetupurl) {
        this.meetupurl = meetupurl;
    }
}
