package com.abbsnabb.meetup.resources;

import static org.assertj.core.api.Assertions.assertThat;

import javax.ws.rs.core.Response;

import com.abbsnabb.meetup.api.Meetup;
import com.abbsnabb.meetup.client.MeetupClient;
import org.junit.Test;

import com.abbsnabb.meetup.client.HttpMeetupClient;

import java.util.Arrays;
import java.util.List;

public class MeetupResourceTest {

    @Test
    public void skalPrinteUtCSV() throws Exception {

        final MeetupResource meetupResource = new MeetupResource(() ->
                Arrays.asList(new Meetup("12", "2015-01-01", "Alt om alle", "Hvem er Mikke Mus?", "http://hei.com")));
        final Response meetups = meetupResource.asCSV();

        final String csv = (String) meetups.getEntity();

        assertThat(csv).startsWith("Dato;Meetupgruppe;Tema");
    }

    @Test
    public void skalReturnereMeetups() throws Exception {
        final MeetupResource meetupResource = new MeetupResource(() ->
                Arrays.asList(new Meetup("12", "2015-01-01", "Alt om alle", "Hvem er Mikke Mus?", "http://hei.com")));
        final List<Meetup> meetups = meetupResource.meetups();

        assertThat(meetups).hasSize(1);
        Meetup meetup = meetups.get(0);
        assertThat(meetup.id).isEqualTo("12");
    }
}
